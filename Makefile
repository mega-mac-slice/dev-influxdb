IMAGE=influxdb:1.6
PORT1=8084
PORT2=8086
PORT3=8090
CONTAINER_ID=$(shell docker container ls | awk '$$2 == "'${IMAGE}'" {print $$1}')

container-id:
	@echo ${CONTAINER_ID}

start:
	docker run -itd  \
	-p ${PORT1}:${PORT1} \
	-p ${PORT2}:${PORT2} \
	-p ${PORT3}:${PORT3} \
	${IMAGE}

stop:
	docker stop ${CONTAINER_ID}

restart:
	docker restart ${CONTAINER_ID}

remove:
	docker container rm --force ${CONTAINER_ID}

check-running:
	@if [ -z "${CONTAINER_ID}" ]; then\
		make start;\
	fi \

status:
	@if [ -z "${CONTAINER_ID}" ]; then\
		echo fail; \
	else \
		echo ok; \
	fi \
